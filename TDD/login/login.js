const Page = require('../core/page');
const ce = require('../core/elements');
const le = require('./elements');
const { ELEMENT_WAIT_TIME } = require('../core/constants');
const { sleep } = require('../core/helper');

class Login extends Page {
  constructor() {
    super();
    this.page1 = new Page();
  }

  async loginWithSuccess(testName) {
    await this.page1.screenshot(testName, `1-${testName}`);
    await this.page1.waitAndClick(le.form, ELEMENT_WAIT_TIME, true);
    await this.page1.screenshot(testName, `2-${testName}`);
    await this.page1.type(le.usernameInput, process.env.METAHUB_USER);
    await this.page1.screenshot(testName, `3-${testName}`);
    await this.page1.type(le.passwordInput, process.env.METAHUB_PASS);
    await this.page1.screenshot(testName, `4-${testName}`);
    await this.page1.selectDropDownElement(le.languageDrpdown, le.optionEnglish);
    await this.page1.screenshot(testName, `5-${testName}`);
    await this.page1.waitAndClick(le.submitBtn, ELEMENT_WAIT_TIME);
    await this.page1.screenshot(testName, `6-${testName}`);
    const peopleMenuTabBtnCheck = await this.page1.hasElement(ce.peopleMenuTabBtn, true, ELEMENT_WAIT_TIME);
    await this.page1.screenshot(testName, `7-${testName}`);
    await sleep(process.env.DEFAULT_SLEEP);
    return peopleMenuTabBtnCheck;
  }

  async loginWithFailure(testName) {
    await this.page1.screenshot(testName, `1-${testName}`);
    await this.page1.waitAndClick(le.form, ELEMENT_WAIT_TIME, true);
    await this.page1.screenshot(testName, `2-${testName}`);
    await this.page1.type(le.usernameInput, process.env.METAHUB_USER);
    await this.page1.screenshot(testName, `3-${testName}`);
    await this.page1.type(le.passwordInput, process.env.METAHUB_WRONG_PASS);
    await this.page1.selectDropDownElement(le.languageDrpdown, le.optionEnglish);
    await this.page1.screenshot(testName, `5-${testName}`);
    await this.page1.waitAndClick(le.submitBtn, ELEMENT_WAIT_TIME);
    await this.page1.screenshot(testName, `6-${testName}`);
    const errorDisplayCheck = await this.page1.hasElement(ce.errorDisplay, true, ELEMENT_WAIT_TIME);
    await this.page1.screenshot(testName, `7-${testName}`);
    await sleep(process.env.DEFAULT_SLEEP);
    return errorDisplayCheck;
  }
}

module.exports = exports = Login;