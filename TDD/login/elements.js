// Login page
exports.form = 'div[class="form"]';
exports.usernameInput = 'input[name="login"]';
exports.passwordInput = 'input[name="password"]';
exports.languageDrpdown = 'select[name="language"]';
exports.optionEnglish = 'option[value="en"]';
exports.submitBtn = 'button[type="submit"]';
