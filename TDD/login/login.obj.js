const Login = require('./login');
const Page = require('../core/page');
const { toMatchImageSnapshot } = require('jest-image-snapshot');
const { TEST_DURATION_TIME } = require('../core/constants'); // core constants (Timeouts vars imported)

expect.extend({ toMatchImageSnapshot });

const loginTest = () => {
  beforeEach(() => {
    jest.setTimeout(TEST_DURATION_TIME);
  });

  test('Realize a Login Successfully', async () => {
    const test = new Login();
    let response;
    let screenshot;
    try {
      const testName = 'loginWithSuccess';
      await test.page1.logger('begin of ', testName);
      await test.page1.init(testName);
      await test.page1.startRecording(testName);
      response = await test.loginWithSuccess(testName);
      await test.page1.logger('end of ', testName);
      await test.page1.stopRecording();
      screenshot = await test.page1.page.screenshot();
    } catch (err) {
      await test.page1.logger(err);
    } finally {
      await test.page1.close();
    }
    expect(response).toBe(true);
    Page.checkRegression(0.1, screenshot);
  });

  test('Realize a failed Login', async () => {
    const test = new Login();
    let response;
    let screenshot;
    try {
      const testName = 'loginWithFailure';
      await test.page1.logger('begin of ', testName);
      await test.page1.init(testName);
      await test.page1.startRecording(testName);
      response = await test.loginWithFailure(testName);
      await test.page1.logger('end of ', testName);
      await test.page1.stopRecording();
      screenshot = await test.page1.page.screenshot();
    } catch (err) {
      await test.page1.logger(err);
    } finally {
      await test.page1.close();
    }
    expect(response).toBe(true);
    Page.checkRegression(0.1, screenshot);
  });
};
module.exports = exports = loginTest;
