## Metahub Puppeteer Tests

Tests for Metahub using Puppeteer, Chromium and Jest.

## Get Metahub URL and configure .env file

Copy the `.env-template` file to a new file, and name it `.env`. In the `.env` file, add your Metahub server URL, so the tests will know which server to connect to.

## Setup

To run these tests, you will need the following:
* Any OS type
* Node.js 8.11.4 or later

To run individual tests, you can also optionally install Jest globally with `sudo npm install jest -g`.

```bash
$ npm install
```

## Running the tests with an existing Metahub server (All in one)

To run all the tests at once, run `./run.sh -t all`.

## Running a single test with an existing Metahub server (Specific test)

To run a specific test from `bash`:

```bash
$ ./run.sh -t testcase
```

Test cases list: `login/all`.

If you have Jest installed globally, you can run individual tests with `jest TEST [TEST...]`. The tests are found in the `.test.js` files, but you may choose to omit file extensions when running the tests.

## Debugging, Recording, Evidences, Metrics and Performance

### Debugging

To debug the tests, you will need to set `DEBUG=true`; if `DEBUG` receives `true`, the logs will show in the console from which we start the tests.

### Recording

To record the tests, you will need to set `WITH_RECORD=true`; if `WITH_RECORD` receives `true`, all tests will be recorded to .mp4 files, to keep track on what's going on and to have a good approach to catch problems or to have a proof that there are no problems.

Recording output will be saved under `data/test-date-testName/recording`; for example: `data/test-19-01-2021-loginWithSuccess/recording`.

**PS:** Recordings are just for manual testing.

### Getting Evidences

Generating evidences is about to take screenshots of the client during testing. And to realize this, assigning `GENERATE_EVIDENCES` in `.env` to `true`. This will take screenshots and save them in `data/test-date-testName/screenshots`; for example: `data/test-19-01-2021-loginWithSuccess/screenshots`.

### Visual Regression

Our test suite includes visual regression specs that can be execute separately with npm run test-visual-regression (desktop only). It will take screenshots of various views and components of the client, save them inside the `./__image_snapshots__` folder and put the failed cases into `./__image_snapshots__/__diff_output__`.

### Metrics

Setting `COLLECT_METRICS` to `true` generates page metrics like the following:

```
    "metricObj": {
        "Timestamp": 508539.87856,
        "Documents": 4,
        "Frames": 1,
        "JSEventListeners": 29,
        "Nodes": 198,
        "LayoutCount": 5,
        "RecalcStyleCount": 6,
        "LayoutDuration": 0.003339,
        "RecalcStyleDuration": 0.017187,
        "ScriptDuration": 0.252291,
        "TaskDuration": 0.685394,
        "JSHeapUsedSize": 15774440,
        "JSHeapTotalSize": 22032384
    },
```

This metrics report will be generated inside `data/test-date-testName/metrics/metrics-date.json` file.

### Performance

Setting `COLLECT_PERFORMANCE` to `true` generates page performance like the following:

```
    "performanceTimingObj": {
        "connectStart": 1640199656657,
        "navigationStart": 1640199656649,
        "loadEventEnd": 0,
        "domLoading": 1640199657115,
        "secureConnectionStart": 0,
        "fetchStart": 1640199656649,
        "domContentLoadedEventStart": 1640199670498,
        "responseStart": 1640199657109,
        "responseEnd": 1640199657111,
        "domInteractive": 1640199670498,
        "domainLookupEnd": 1640199656657,
        "redirectStart": 0,
        "requestStart": 1640199656881,
        "unloadEventEnd": 0,
        "unloadEventStart": 0,
        "domComplete": 0,
        "domainLookupStart": 1640199656657,
        "loadEventStart": 0,
        "domContentLoadedEventEnd": 1640199670498,
        "redirectEnd": 0,
        "connectEnd": 1640199656881
    }
```

This Performance report will be generated inside `data/test-date-testName/metrics/metrics-date.json` file.
