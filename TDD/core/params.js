require('dotenv').config();

module.exports = exports =
{
  server: process.env.BASE_URL,
  username: process.env.METAHUB_USER,
  password: process.env.METAHUB_PASS
};
