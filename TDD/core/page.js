require('dotenv').config();
const puppeteer = require('puppeteer');
const fs = require('fs');
const fsExtra = require('fs-extra');
const moment = require('moment');
const path = require('path');
const PuppeteerVideoRecorder = require('puppeteer-video-recorder');
const helper = require('./helper');
const params = require('./params');
const { ELEMENT_WAIT_TIME } = require('./constants');
const { gatherPerformanceTimingMetric,
  gatherPerformanceTimingMetrics,
  processPerformanceTimingMetrics } = require('./helper');

class Page {
  constructor(page) {
    this.page = page;
    this.screenshotIndex = 0;
    this.parentDir = this.getParentDir(__dirname);
    this.recorder = new PuppeteerVideoRecorder();
  }

  getParentDir(dir) {
    const tmp = dir.split('/');
    tmp.pop();
    return tmp.join('/');
  }

  // Init Metahub session
  async init(testName) {
    try {
      this.today = moment(new Date()).format('DD-MM-YYYY');
      const args = this.getArgs();
      this.effectiveParams = Object.assign({}, params);
      if (process.env.BROWSERLESS_ENABLED === 'true') {
        this.browser = await puppeteer.connect({
          browserWSEndpoint: `ws://${process.env.BROWSERLESS_URL}?token=${process.env.BROWSERLESS_TOKEN}&${args.args.join('&')}`,
        });
      } else {
        this.browser = await puppeteer.launch(args);
      }
      this.page = await this.browser.newPage();

      this.page.setDefaultTimeout(3600000);

      // Getting all page console logs
      if (process.env.PAGE_LOG === 'true') {
        this.page.on('console', async msg => console[msg._type](
          ...await Promise.all(msg.args().map(arg => arg.jsonValue()))
        ));
      }

      await this.page.setExtraHTTPHeaders({
        'Accept-Language': `${process.env.LANG}`,
      });

      await this.page.goto(params.server, { waitUntil: 'networkidle2' });
      if (process.env.COLLECT_METRICS === 'true') {
        await this.getMetrics(testName);
      }
      if (process.env.COLLECT_PERFORMANCE === 'true') {
        const rawMetrics = await gatherPerformanceTimingMetrics(this.page);
        const metrics = await processPerformanceTimingMetrics(rawMetrics);
        this.logger("PerformanceTiming:",
          {
            "DNS": metrics.dnsLookup,
            "TCP": metrics.tcpConnect,
            "Req": metrics.request,
            "Res": metrics.response,
            "DOM load": metrics.domLoaded,
            "DOM interactive": metrics.domInteractive,
            "Document load": metrics.pageLoad,
            "Full load time": metrics.fullTime
          }
        );
        const loadEventEnd = await gatherPerformanceTimingMetric(this.page, 'loadEventEnd');
        const date = new Date(loadEventEnd);
        console.log(`Page load ended on: ${date}`);
      }
    } catch (err) {
      await this.logger(err);
    }
  }

  // Regression testing
  static checkRegression(numb, screenshot) {
    if (process.env.REGRESSION_TESTING === 'true') {
      expect(screenshot).toMatchImageSnapshot({
        failureThreshold: numb,
        failureThresholdType: 'percent',
      });
    }
  }

  // Run the test for the page
  async test() {
  }

  // Closes the page
  async close() {
    await this.browser.close();
  }

  // Gets the DOM elements being tested, as strings
  async getTestElements() {
  }

  async returnElement(element) {
    return document.querySelectorAll(element)[0];
  }

  // Get the default arguments for creating a page
  getArgs() {
    if (process.env.BROWSERLESS_ENABLED === 'true') {
      const args = [
        '--no-sandbox',
        '--use-fake-ui-for-media-stream',
        '--use-fake-device-for-media-stream',
        '--window-size=1024,720',
        `--lang=${process.env.LANG}`,
      ];
      return {
        headless: true,
        args,
      };
    }
    const args = [
      '--no-sandbox',
      '--use-fake-ui-for-media-stream',
      '--use-fake-device-for-media-stream',
      '--no-default-browser-check',
      '--window-size=1150,980',
      '--allow-file-access',
      `--lang=${process.env.LANG}`,
      '--disable-features=IsolateOrigins,site-per-process',
    ];
    return {
      headless: !+process.env.SHOW_BROWSER,
      args,
      defaultViewport: {
        width: 1250,
        height: 850,
      },
      ignoreDefaultArgs: [
        '--enable-automation',
      ],
    };
  }

  // Returns a Promise that resolves when an element does not exist/is removed from the DOM
  async waitForElementHandleToBeRemoved(element, timeout = ELEMENT_WAIT_TIME) {
    await this.page.waitForSelector(element, { timeout, hidden: true });
  }

  async wasRemoved(element, timeout = ELEMENT_WAIT_TIME) {
    try {
      await this.waitForElementHandleToBeRemoved(element, timeout);
      return true;
    } catch (err) {
      this.logger(err);
      return false;
    }
  }

  async hasElement(element, visible = true, timeout = ELEMENT_WAIT_TIME) {
    try {
      await this.page.waitForSelector(element, { visible, timeout });
      return true;
    } catch (err) {
      await this.logger(err);
      return false;
    }
  }

  async selectDropDownElement(select, option) {
    await this.page.select(select, option);
  }

  // Presses the Enter key
  async enter() {
    await this.page.keyboard.press('Enter');
  }

  // Press a keyboard button
  async press(key) {
    await this.page.keyboard.press(key);
  }

  async bringToFront() {
    await this.bringToFront();
  }

  async getLastTargetPage() {
    const browserPages = await this.browser.pages();
    return new Page(browserPages[browserPages.length - 1]);
  }

  async waitAndClick(element, timeout = ELEMENT_WAIT_TIME, relief = false) {
    if (relief) await helper.sleep(1000);
    await this.waitForSelector(element, timeout);
    await this.page.focus(element);
    await this.page.click(element, true);
  }

  async waitAndClickElement(element, index = 0, timeout = ELEMENT_WAIT_TIME, relief = false) {
    if (relief) await helper.sleep(1000);
    await this.waitForSelector(element, timeout);
    await this.page.evaluate((elem, i) => {
      document.querySelectorAll(elem)[i].click();
    }, element, index);
  }

  async clickNItem(element, n, relief = false) {
    if (relief) await helper.sleep(1000);
    await this.waitForSelector(element);
    const elementHandle = await this.page.$$(element);
    await elementHandle[n].click();
  }

  async type(element, text, relief = false) {
    if (relief) await helper.sleep(1000);
    await this.waitForSelector(element);
    await this.page.focus(element);
    await this.page.type(element, text);
  }

  async startRecording(testName) {
    if (process.env.WITH_RECORD === 'true') {
      const finalSaveFolder = path.join(__dirname, `../${process.env.TEST_FOLDER}`);
      if (!fs.existsSync(finalSaveFolder)) {
        fs.mkdirSync(finalSaveFolder);
      }
      this.testNameFolder = `${finalSaveFolder}/test-${this.today}-${testName}`;
      if (!fs.existsSync(this.testNameFolder)) {
        fs.mkdirSync(this.testNameFolder);
      }
      await this.recorder.init(this.page, `${this.testNameFolder}/recording`);
      await this.recorder.start();
    }
  }

  async stopRecording() {
    if (process.env.WITH_RECORD === 'true') {
      await this.recorder.stop();
      await helper.sleep(5000);
      await this.removeRecordingImages();
    }
  }

  async removeRecordingImages() {
    await fs.unlinkSync(`${this.testNameFolder}/recording/images.txt`);
    await helper.sleep(5000);
    await fsExtra.removeSync(`${this.testNameFolder}/recording/images`);
  }

  async screenshot(testName, testFileName, relief = false) {
    if (process.env.GENERATE_EVIDENCES === 'true') {
      const dir = path.join(__dirname, `../${process.env.TEST_FOLDER}`);
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      const testResultsFolder = `${dir}/test-${this.today}-${testName}`;
      if (!fs.existsSync(testResultsFolder)) {
        fs.mkdirSync(testResultsFolder);
      }
      const screenshots = `${testResultsFolder}/screenshots`;
      if (!fs.existsSync(screenshots)) {
        fs.mkdirSync(screenshots);
      }
      if (relief) await helper.sleep(1000);
      const filename = `${testFileName}.png`;
      await this.page.screenshot({ path: `${screenshots}/${filename}` });
      this.screenshotIndex++;
      return testResultsFolder;
    }
  }

  async logger() {
    if (process.env.DEBUG === 'true') {
      const date = moment(new Date()).format('DD-MMM-YYYY HH:mm:ss');
      const args = Array.prototype.slice.call(arguments);
      args.unshift(`${date} `);
      console.log(...args);
    }
  }

  async paste(element) {
    await this.waitAndClick(element);
    await this.page.keyboard.down('ControlLeft');
    await this.page.keyboard.press('KeyV');
    await this.page.keyboard.up('ControlLeft');
  }

  async waitForSelector(element, timeout = ELEMENT_WAIT_TIME) {
    await this.page.waitForSelector(element, { timeout });
  }

  async getMetrics(testName) {
    const pageMetricsObj = {};
    const dir = path.join(__dirname, `../${process.env.TEST_FOLDER}`);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    const testExecutionResultsName = `${dir}/test-${this.today}-${testName}`;
    if (!fs.existsSync(testExecutionResultsName)) {
      fs.mkdirSync(testExecutionResultsName);
    }
    const metricsFolder = `${testExecutionResultsName}/metrics`;
    if (!fs.existsSync(metricsFolder)) {
      fs.mkdirSync(metricsFolder);
    }
    const performanceTiming = JSON.parse(
      await this.page.evaluate(() => JSON.stringify(window.performance.timing))
    );
    const metric = await this.page.metrics();
    pageMetricsObj['metricObj'] = metric;
    pageMetricsObj['performanceTimingObj'] = performanceTiming;
    const metricsFile = path.join(__dirname, `../${process.env.TEST_FOLDER}/test-${this.today}-${testName}/metrics/metrics-${this.today}.json`);
    const createFile = async () => {
      try {
        fs.writeFileSync(metricsFile, `${JSON.stringify(pageMetricsObj, undefined, 4)},\n`);
      } catch (err) {
        await this.logger(err);
      }
    };
    await createFile();
  }
}

module.exports = exports = Page;