# CodeceptJS - Metahub
A Metahub testing framework coded in CodeceptJS and Puppeteer.

## Requirements
for NodeJs: v10.19.0
for NPM: v6.14.15

## Metahub steps :: install

```
npm install
npm install semver
```
## :: Environment

````

Create a copy of .env-template and rename to .env
Insert IP address and users informations

## :: execution

```
env $(cat .env | xargs) npx codeceptjs run
```