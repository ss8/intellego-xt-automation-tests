const dotenv = require('dotenv');
const dotenvParseVariables = require('dotenv-parse-variables');

let env = dotenv.config({})
if (env.error) throw env.error;
env = dotenvParseVariables(env.parsed);

exports.config = {
	output: './output',
	helpers: {
		Puppeteer: {
			url: env.BASE_URL,
			show: env.SHOW_BROWSER,
			windowSize: '1200x900',
			waitForNavigation: "networkidle0",
			getPageTimeout:0,
			waitForAction: 700,
			slowMo: 250,
			chrome: {
				"args": ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage', '--ignore-certificate-errors']
			}
		},
		FileSystem: {},
		CustomHelper: {
			require: './support/helpers/custom_helper.js',
		},
	},
	include: {
		I: './steps_file.js',
		LoginPage: './page_objects/login_page.js',
		HomePage: './page_objects/home_page.js',
		DataSourcePage: './page_objects/dataSource_page.js',
	},
	mocha: {},
	bootstrap: null,
	teardown: null,
	hooks: [],
	gherkin: {
		features: './features/*/*.feature'
	},
	plugins: {
		allure: {
			enabled: true,
			outputDir: "./output/allure/allure-results"
		},
		screenshotOnFail: {
			enabled: true
		},
		skipper: {
			require: './support/plugins/skipper.js',
			enabled: true
		},
	},
	tests: './step_definitions/*/*_test.js',
	name: 'e2e'
}
