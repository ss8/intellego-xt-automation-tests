class Singleton {
  constructor() {
  }
  getInstance() {
    return Singleton.instance;
  }
}

module.exports = Singleton;
