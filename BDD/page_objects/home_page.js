const BasePage = require('./base_page.js');
const { I } = inject();
const timeout = 20;
class HomePage extends BasePage {
	constructor() {
		super();
		this.peopleTabBtn = '#people';
	}

	async asyncisCurrentPage() {
		await super.isCurrentPage();
		await super.waitForLoaderInvisible();
	}

	async isRightPage() {
		return await I.usePuppeteerTo('find element', async ({ page })=>{
			await page.evaluate(async ()=> await document.getElementById('people') !== "") === true;
		});
	}
}

module.exports = new HomePage();
module.exports.HomePage = HomePage;
