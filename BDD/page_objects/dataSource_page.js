const { I } = inject();
const BasePage = require('./base_page.js');


class DataSourcePage extends BasePage {
	constructor() {
		super();
		this.importMenu = '#import';
		this.newDataSourceButton = 'button[class="bp3-button"]';
		this.dataSourceNameImput = '#data-source-input-name';
		this.dataSourceDescription = 'input[name="desc"]';
		this.dataSourceTypeDropDown = 'select[name="datasourceType"]';
		this.eventTypeDropDown = "//span[contains(text(),'Select Type')]";
		this.eventTypeInputSearch = 'button[class="bp3-input"]';
		this.SelectOptionField = '//div[contains(@class,"bp3-text-overflow-ellipsis bp3-fill") and text()="<VALUE>"]';
		this.uploadDataSourceFile = '#uploadFile';
		this.tableColumn = '//div[contains(@class,"ag-cell ag-cell-not-inline-editing ag-cell-auto-height ag-cell-value") and text()="<VALUE>"]';
		this.fieldValue = "//div[@role='option'][text()='<VALUE>']";
		this.dataTypeClick = "(//div[@tabindex='-1'][text()='Number'])[2]";
		this.mappedFieldClickIMSI = "(//div[@tabindex='-1'])[20]";
		this.mappedFieldClickA_SUBS = "(//div[@tabindex='-1'])[27]";
		this.mappedFieldClickIMEI = "(//div[@tabindex='-1'])[34]";
		this.mappedFieldClickDateTime = "(//div[@tabindex='-1'])[62]";
		this.dataTypeClickDateTime = "(//div[@tabindex='-1'][text()='Number'])[5]";
		this.dataTypeClickB_SBS = "(//div[@tabindex='-1'][text()='Number'])[6]";
		this.mappedFieldClickB_SBS = "(//div[@tabindex='-1'])[76]";
		this.mapAsField = "(//div[@tabindex='-1'][text()='<VALUE>'])";
		this.buttons = "//span[@class='bp3-button-text'][text()='<VALUE>']";
		this.fieldTableDataSource = "//div[@tabindex='-1'][text()='<VALUE>']"
		this.toastMessage = "//span[@class='bp3-toast-message'][text()='<VALUE>']";
		this.threeDots = "//span[@class='bp3-icon bp3-icon-more']";

	}
}

module.exports = new DataSourcePage();
module.exports.DataSourcePage = DataSourcePage;
