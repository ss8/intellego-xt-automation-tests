const { I } = inject();
const timeout = 60;

class BasePage {
	constructor() {
		this.timeout = timeout;

	}

	async isCurrentPage(mainElement = null) {
		await I.waitForElement(mainElement || this.pageTitle, this.timeout);
	}

	async fillField(field, value, timeout = this.timeout) {
		await I.waitForElement(field, timeout);
		await I.fillField(field, value);
	}

	async click(field, timeout = this.timeout) {
		await I.waitForElement(field, timeout);
		await I.click(field);
	}

	async doubleClick(field, timeout = this.timeout) {
		await I.waitForElement(field, timeout);
		await I.doubleClick(field);
	}

	async waitForLoaderInvisible(timeout = 30) {
		await I.waitForInvisible(this.loader, timeout);
	}

	async selectOption(field, value, timeout = this.timeout) {
		await I.waitForElement(field, timeout);
		await I.selectOption(field, value);
	}

	async attachFile(field, file, timeout = this.timeout) {
		await I.waitForElement(field, timeout);
		await I.attachFile(field, file);
	}
}

module.exports = BasePage;