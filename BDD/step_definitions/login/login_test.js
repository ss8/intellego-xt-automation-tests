
const { I, LoginPage, HomePage } = inject();
const timeout = 20;
const assert = require('assert');

//#region .: Realize login to Metahub :.

Given('I am already in the Metahub login page', async () => {
	await LoginPage.accessLoginPage();
});

Given('I fill the fields username and password', async () => {
	await LoginPage.fillUserAndPass(process.env.USER, process.env.PASSWORD);
});

When('I click submit', async () => {
	await LoginPage.clickLogin();
});

Then('I can see the Metahub Home Page', async () => {
	await HomePage.isRightPage();
});

//#endregion

//Scenario outline: Wrong password or username

When('I fill the field Username with {string} and Password with {string}', async (username, password) => {
	await LoginPage.fillField(LoginPage.userInput, username);
	await LoginPage.fillField(LoginPage.passwordInput, password);
});

Then('I can see the message {string}', async (message) => {
	await I.waitForElement(LoginPage.errorMessageLogin);
	const value = await I.grabTextFrom(LoginPage.errorMessageLogin);
	resultValue = value.trim();
	assert.equal(resultValue, message);
});

//#endregion