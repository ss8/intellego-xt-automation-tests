
const { I, LoginPage, HomePage, DataSourcePage } = inject();
const timeout = 20;

//#region .: Importing a new CDR data source file with success :.

Given('I am logged in metahub', async () => {
	await LoginPage.accessLoginPage();
	await LoginPage.fillUserAndPass(process.env.USER, process.env.PASSWORD);
	await LoginPage.clickLogin();
});

Given('I am already in the import page', async () => {
	await DataSourcePage.click(DataSourcePage.importMenu);
});

When('I click in the New Data Source button', async () => {
	await DataSourcePage.click(DataSourcePage.newDataSourceButton);
});

When('I fill the field Data Source Name with the value {string}', async (dataSourceInput) => {
	await DataSourcePage.fillField(DataSourcePage.dataSourceNameImput, dataSourceInput);
});

When('I fill the field Data Source Description with the value {string}', async (dataSourceDescription) => {
	await DataSourcePage.fillField(DataSourcePage.dataSourceDescription, dataSourceDescription);
});

When('I select the option {string} in the Data Source Type field', async (dataSourceType) => {
	await DataSourcePage.selectOption(DataSourcePage.dataSourceTypeDropDown, dataSourceType);
});

When('I select the option {string} in the Event Type field', async (eventType) => {
	await DataSourcePage.click(DataSourcePage.eventTypeDropDown);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace('<VALUE>', eventType), timeout);
	const filepath = './support/files/cdr_2021-11-23_17-50-45-736745.csv';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});

When('I select the option {string} in the column Data Type and {string} in the column Mapped Field, both in the line with IMSI Field Name', async (dataType, mappedField) => {
	await DataSourcePage.doubleClick(DataSourcePage.dataTypeClick);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
	await DataSourcePage.doubleClick(DataSourcePage.mappedFieldClickIMSI);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', mappedField), timeout);
});

When('I select the option {string} in the column Data Type and {string} in the column Mapped Field, both in the line with A_SUBS Field Name', async (dataType, mappedField) => {
	await DataSourcePage.doubleClick(DataSourcePage.dataTypeClick);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
	await DataSourcePage.doubleClick(DataSourcePage.mappedFieldClickA_SUBS);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', mappedField), timeout);
});

When('I select the option {string} in the column Data Type and {string} in the column Mapped Field, both in the line with IMEI Field Name', async (dataType, mappedField) => {
	await DataSourcePage.doubleClick(DataSourcePage.dataTypeClick);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
	await DataSourcePage.doubleClick(DataSourcePage.mappedFieldClickIMEI);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', mappedField), timeout);
});

When('I select the option {string} in the column Data Type and {string} in the column Mapped Field, both in the line with Datetime Field Name', async (dataType, mappedField) => {
	await DataSourcePage.doubleClick(DataSourcePage.dataTypeClickDateTime);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
	await DataSourcePage.doubleClick(DataSourcePage.mappedFieldClickDateTime);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', mappedField), timeout);
});

When('I select the option {string} in the column Data Type and {string} in the column Mapped Field, both in the line with B_SUBS Field Name', async (dataType, mappedField) => {
	await DataSourcePage.doubleClick(DataSourcePage.dataTypeClickB_SBS);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
	await DataSourcePage.doubleClick(DataSourcePage.mappedFieldClickB_SBS);
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', mappedField), timeout);
});

When('I select the option {string} in the column Map As in the line with MCC Field Name', async (dataType) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', dataType), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
});

When('I select the option {string} in the column Map As in the line with MNC Field Name', async (dataType) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', dataType), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
});

When('I select the option {string} in the column Map As in the line with LAC Field Name', async (dataType) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', dataType), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
});

When('I select the option {string} in the column Map As in the line with {string} Field Name', async (dataType, fieldName) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', fieldName), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', dataType), timeout);
});

When('I select the checkbox of the fields {string}, {string}, {string} and {string}', async (MCC, MNC, LAC, CELLID) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', MCC), timeout);
	I.pressKey('Space');
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', MNC), timeout);
	I.pressKey('Space');
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', LAC), timeout);
	I.pressKey('Space');
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', CELLID), timeout);
});

When('I click in the button {string}', async (button) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace('<VALUE>', button), timeout);
});

Then('I can see the toast message {string}', async (message) => {
	await I.waitForElement(DataSourcePage.toastMessage.replace('<VALUE>', message), timeout);
});

Then('I can see the {string}, {string}, {string}, {string}, {string}, {string} and {string} informations', async (dataSourceName, description, fileType, createdBy, totalRecords, failedRecords, totalImports) => {
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', dataSourceName), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', description), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', fileType), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', createdBy), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', totalRecords), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', failedRecords), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', totalImports), timeout);
});

//#endregion

//#region .: Deleting a CDR Data Source file with success :.

When('I already have a file with {string} imported', async (fileName) => {
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', fileName), timeout);
});

When('I click in the option {string} and confirm pressing the option {string}', async (deleteButton, yesOption) => {
	await DataSourcePage.click(DataSourcePage.threeDots);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace('<VALUE>', deleteButton), timeout);
	await DataSourcePage.click(DataSourcePage.buttons.replace('<VALUE>', yesOption), timeout);
});

//#endregion
