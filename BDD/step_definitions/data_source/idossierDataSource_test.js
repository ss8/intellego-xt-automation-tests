const { I, LoginPage, HomePage, DataSourcePage } = inject();
const timeout = 20;

//#region .: Importing a new iDossier data source file with success :

When('I click in the {string} menu', async (button) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace('<VALUE>', button), timeout);
});

When('I click in the {string} button', async (button) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace('<VALUE>', button), timeout);
});

When('I select the file to be imported', async () => {
	const filepath = './support/files/test_idossier.csv';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});

When('I select the option {string} in the Mapped Field column at the line {string}', async (name, line) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', line), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', name), timeout);
});

When('I select the option {string} in the Information Type column and {string} in the column Category at the line {string}', async (text, category, line) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', line), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', text), timeout);
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', category), timeout);
});

When('I select the option {string} in the Data Type column and {string} in the column Information Type and {string} in the column Category at the line {string}', async (text, type, info, line) => {
	await DataSourcePage.click(DataSourcePage.mapAsField.replace('<VALUE>', line), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', text), timeout);
	I.pressKey('Tab');
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', type), timeout);
	I.pressKey('Tab');
	I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.fieldValue.replace('<VALUE>', info), timeout);
});

Then('I can see the {string}, {string} and {string} informations', async (dataSourceName, description, createdBy) => {
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', dataSourceName), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', description), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', createdBy), timeout);
});

//#endregion

//#region .: Deleting a Idossier Data Source file with success :.

When('I already have a file with {string} name imported', async (fileName) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace('<VALUE>', "iDossier Data Sources"), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace('<VALUE>', fileName), timeout);
});

//#endregion
