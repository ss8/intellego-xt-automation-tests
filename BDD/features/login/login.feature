#language: en

@login
Feature: Verify the Login Screen
    As a Metahub user
    Wants to login to Metahub with username and password
    So I can get access to all the existing resources

Scenario: Realize login to Metahub with success
    Given I am already in the Metahub login page
    And I fill the fields username and password
    When I click submit
    Then I can see the Metahub Home Page

Scenario Outline: Wrong password or username
    Given I am already in the Metahub login page
    And I fill the field Username with "<USERNAME>" and Password with "<PASSWORD>"
    When I click submit
    Then I can see the message "<MESSAGE>"
Examples:
       | USERNAME  | PASSWORD  | MESSAGE                                |
       | grpadm    | 1234      | Wrong password or username. Try again. |
       | abcd      | 1234      | Wrong password or username. Try again. |
