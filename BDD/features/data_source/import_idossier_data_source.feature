#language: en

@datasource
@idossierdatasource
Feature: Data Source managment
    As a Metahub user
    Wants to manage data source information 
    So I can import idossier data

Background:
    Given I am logged in metahub

Scenario Outline: Importing a new iDossier data source file with success
    Given I am already in the import page
    When I click in the "iDossier Data Sources" menu
    And I click in the "New Data Source" button
    And I fill the field Data Source Name with the value "iDossier"
    And I fill the field Data Source Description with the value "iDossier_file_test"
    And I select the file to be imported
    And I select the option "name" in the Mapped Field column at the line "Name"
    And I select the option "Free Text" in the Information Type column and "Basic Info" in the column Category at the line "Hobbies"
    And I select the option "Text" in the Data Type column and "Mobile Phone" in the column Information Type and "Basic Info" in the column Category at the line "Mobile Phone"
    And I select the option "profileImage" in the Mapped Field column at the line "Profile Image"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    And I can see the "<DATA SOURCE NAME>", "<DESCRIPTION>" and "<CREATED BY>" informations
Examples:
       | DATA SOURCE NAME  | DESCRIPTION        |  CREATED BY  |
       |   iDossier        | iDossier_file_test |   vdias      |

Scenario: Deleting a Idossier Data Source file with success
    Given I am already in the import page
    And I already have a file with "iDossier" name imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "iDossier  successfully deleted."
