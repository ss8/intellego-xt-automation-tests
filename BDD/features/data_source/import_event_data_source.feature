#language: en

@datasource
@eventdatasource
Feature: Data Source managment
    As a Metahub user
    Wants to manage data source information 
    So I can import event data

Background:
    Given I am logged in metahub

Scenario Outline: Importing a new CDR data source file with success
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "CDR_FILE"
    And I fill the field Data Source Description with the value "CDR_FILE_TEST"
    And I select the option "Regular" in the Data Source Type field
    And I select the option "CDR" in the Event Type field
    And I select the option "Text" in the column Data Type and "IMSI" in the column Mapped Field, both in the line with IMSI Field Name
    And I select the option "Text" in the column Data Type and "Sender" in the column Mapped Field, both in the line with A_SUBS Field Name
    And I select the option "Text" in the column Data Type and "IMEI" in the column Mapped Field, both in the line with IMEI Field Name
    And I select the option "DateTime" in the column Data Type and "Event Date" in the column Mapped Field, both in the line with Datetime Field Name
    And I select the option "Text" in the column Data Type and "Recipient" in the column Mapped Field, both in the line with B_SUBS Field Name
    And I select the option "MCC" in the column Map As in the line with MCC Field Name
    And I select the option "MNC" in the column Map As in the line with MNC Field Name
    And I select the option "LAC" in the column Map As in the line with LAC Field Name
    And I select the option "CellID" in the column Map As in the line with "CELLID" Field Name
    And I select the checkbox of the fields "MCC", "MNC", "LAC" and "CellID"
    And I click in the button "Merge Fields"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    Then I can see the "<DATA SOURCE NAME>", "<DESCRIPTION>", "<FILE TYPE>", "<CREATED BY>", "<TOTAL RECORDS>", "<FAILED RECORDS>" and "<TOTAL IMPORTS>" informations
Examples:
       | DATA SOURCE NAME  | DESCRIPTION   | FILE TYPE |  CREATED BY  |  TOTAL RECORDS  |  FAILED RECORDS  | TOTAL IMPORTS  |
       |     CDR_FILE      | CDR_FILE_TEST |   CSV     |   vdias      |       0         |        0         |      0         |     

Scenario: Deleting a CDR Data Source file with success
    Given I am already in the import page
    And I already have a file with "CDR_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
